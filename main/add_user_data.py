from .database_io.inputs.mock.getUsers import mockInput
from .database_io.outputs.neo4j.exportData import neo4jOutput
from .platforms.youtube import youtube
from .platforms.spotify import spotify

import pprint
import asyncio


platform_crawlers = {
    "youtube": youtube.youtubePlatform(lambda x: neo4jOutput().export(x)).get_media,
    "spotify": spotify.spotifyPlatform(lambda x: neo4jOutput().export(x)).get_media,

    #     "youtube": youtube.youtubePlatform(lambda x: pprint.PrettyPrinter(indent=4).pprint(x)).get_media,
    #     "spotify": spotify.spotifyPlatform(lambda x: pprint.PrettyPrinter(indent=4).pprint(x)).get_media,

}


def log_dict_error(platform_name):
    print("platform missing: ", platform_name)


async def add_user_data(users):
    print("hello")
    assert isinstance(users, list)
    assert isinstance(users[0], dict)
    for user in users:
        user_id = user["user_id"]
        for platform in user["platforms"]:
            platform_name, platform_token, refresh_token = platform[
                "platform_name"], platform["platform_token"], platform["refresh_token"]
            if platform_name in platform_crawlers:
                asyncio.ensure_future(platform_crawlers[platform_name](
                    user_id, platform_token, refresh_token))
                platform_crawlers[platform_name](
                    user_id, platform_token, refresh_token)
            else:
                log_dict_error(platform_name)


def start(user_access_tokens):

    # mock_getUsers = mockInput().getUsers()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(add_user_data(user_access_tokens))
    pending = asyncio.Task.all_tasks()
    loop.run_until_complete(asyncio.gather(*pending))
