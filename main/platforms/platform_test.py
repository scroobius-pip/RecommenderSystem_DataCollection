# from .spotify import spotify,mock_spotify
from .youtube import youtube,mock_youtube
from .types_constants import CONST_MUSIC,CONST_VIDEO
youtubePlatform = youtube.youtubePlatform()
# spotifyPlatform = spotify.spotifyPlatform()


YOUTUBE_ACCESS_TOKEN = "ya29.Glt0BXgYozxjEWET9SlpUrsdEcRoOvhtqEcFUg7pvPif1Fj-0WMDuium0PFuCf0vbpVQ2nOZul-mXAr0jS6xcZUY_NMR4-jC9OlifhR2cz0K2ep4vNlEUwRnEqL0"
def test_retrieve():
    youtube_data = youtubePlatform.retrieve("",YOUTUBE_ACCESS_TOKEN)
    print(youtube_data)
    # f = open("youtube_data.txt","w")
    # pickle.dump(str(youtube_data),f)
    # spotify_data = spotifyPlatform.retrieve("","")
    "retrieve() should return a list of dictionaries"
    assert isinstance(youtube_data,list),"youtubePlatform.retrieve() should return a list"
    assert isinstance(youtube_data[0],dict)
    # assert isinstance(spotify_data,list)
    # assert isinstance(spotify_data[0],dict)
    

    "retrieve() should return None when arguments are missing"
    assert youtubePlatform.retrieve() == None
    assert youtubePlatform.retrieve("") == None
    # assert spotifyPlatform.retrieve() == None
    # assert spotifyPlatform.retrieve("") == None

    """id and title should exist in dictionary in list returned by retrieve() function"""
    assert "id" in youtube_data[0]
    assert "title" in youtube_data[0]
    # assert "id" in spotify_data[0]
    # assert "title" in spotify_data[0]
    


def test_transform():
    
    #Youtube
    retrieved_data_youtube_mock = {
   "id": "ZZSmo4A37KI",
    "title": "Mahou Shoujo Site / Magical Girl Site  | Trailer | TV Anime PV-1 2018"
    }
    youtube_data_transformed = youtubePlatform.transform(retrieved_data_youtube_mock)
    print(youtube_data_transformed)
    del youtube_data_transformed["id"] #id will always be different
    assert youtube_data_transformed == {
        "name":"Mahou Shoujo Site / Magical Girl Site  | Trailer | TV Anime PV-1 2018",
        "type": CONST_VIDEO,
        "source":"youtube",
        "url":"www.youtube.com/watch?v=ZZSmo4A37KI"

    }
    
    #Spotify
    retrieved_data_spotify_mock = {
   "id": "6z88n0MKRG2UBlUlzvaqbS",
    "title": "Robbin the Rich"
    }
    spotify_data_transformed = spotifyPlatform.transform(retrieved_data_spotify_mock)
    # print(youtube_data_transformed)
    del spotify_data_transformed["id"] #id will always be different
    assert spotify_data_transformed == {
        "name":"Robbin the Rich",
        "type": CONST_MUSIC,
        "source":"spotify",
        "url":"https://open.spotify.com/track/6z88n0MKRG2UBlUlzvaqbS"

    }


def test_get_media():
    # youtubePlatform.get_media("","")
    # spotifyPlatform.get_media("","")
    assert isinstance(youtubePlatform.get_media("",""),tuple)
    assert isinstance(youtubePlatform.get_media("","")[0],list)
    assert isinstance(youtubePlatform.get_media("","")[1],str)
    assert youtubePlatform.get_media("simdi","")[1] == "simdi"

    # assert isinstance(spotifyPlatform.get_media("",""),tuple)
    # assert isinstance(spotifyPlatform.get_media("","")[0],list)
    # assert isinstance(spotifyPlatform.get_media("","")[1],str)
    # assert spotifyPlatform.get_media("simdi","")[1] == "simdi"

