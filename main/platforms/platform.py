from interface import Interface
from typing import List,Dict,Tuple

class Platform(Interface):
    # """ Used internally by the retrieve function , it returns """
    # def _collect_pages(self)->bool:
    #     pass

    def _id_generator(self,retrieved_data)->str:
        """ Used internally by the transform function to generate id """
        pass
        
    def _name_generator(self,retrieved_data)->str:
        """ Used internally by the transform function to generate name """
        pass

    def _type_generator(self)->str:
        """ Used internally by the transform function to generate type """
        pass

    def _source_generator(self)->str:
        """ Used internally by the transform function to generate the source"""
        pass

    def _url_generator(self,retrieved_data)->str:
        """ Used internally by the transform function to generate url of media """
        pass

    def retrieve(self, platform_user_id:str=None,platform_token:str=None)->List[Dict]:
        """ This function is used to retrieve information using the platforms api """
        
        pass

    def transform(self,retrieved_data:dict)->Dict:
        """
        This function is used to transform data from the retrieve function to the unified data structure. 
        Unified Data Structure (UDF)
         __________
        |          |
        | ID       |
        | NAME     |
        | TYPE     |
        | SOURCE   |
        | URL      |  
        |__________|
        
        """
        pass

    def get_media(self, platform_user_id:str,platform_token:str)->Tuple[List[Dict],str]:
        """ This function composes all the other functions and returns the UDF and user_id"""
        
        pass

  
        
 
