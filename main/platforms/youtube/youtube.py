from interface import implements
from googleapiclient.discovery import build
from oauth2client import GOOGLE_TOKEN_URI
import google.oauth2.credentials
from google.auth.exceptions import RefreshError
from google.auth.transport import Request as googleRequest
from ..hash_gen import genHash
from ..platform import Platform
from .mock_youtube import mock
from ..types_constants import CONST_VIDEO

import pprint


class youtubePlatform():

    def __init__(self, callback=lambda x:  pprint.PrettyPrinter(indent=4).pprint(x)):
        self.callback = callback

    def _id_generator(self, retrieved_data):

        return genHash(retrieved_data["id"])

    def _name_generator(self, retrieved_data):
        return retrieved_data["title"]

    def _type_generator(self):

        return CONST_VIDEO

    def _source_generator(self):
        return "youtube"

    def _tags_generator(self, retrieved_data):
        return [element.lower() for element in retrieved_data["tags"]]

    def _url_generator(self, retrieved_data):
        return "www.youtube.com/watch?v="+retrieved_data["id"]

    def retrieve(self, platform_user_id: str=None, platform_token: str=None, refresh_token: str=None):
        if(platform_user_id == None or platform_token == None or refresh_token == None):
            return None

        access_token = platform_token
        token_expiry = None
        token_uri = GOOGLE_TOKEN_URI
        user_agent = 'Python client library'
        revoke_uri = None
        credentials = google.oauth2.credentials.Credentials(
            token=access_token,
            client_id="370795417709-q4mr21i7qc3qu8rkj2f6d4oi3ca96n9p.apps.googleusercontent.com",
            client_secret="zFijngmVU8PIyi8KYq1J3qs0",
            refresh_token=refresh_token,
            token_uri=token_uri,


        )
        # credentials.refresh(googleRequest)
        youtube = build('youtube', 'v3', credentials=credentials)
        videos = youtube.videos()

        request = videos.list(
            part="id,snippet",
            maxResults=50,
            fields="items(id,snippet(tags,title)),nextPageToken",
            myRating="like"

        )

        videos_list = []
        try:
            for x in range(10):
                if request is None:
                    break
                response = request.execute()
                videos_list.append(response["items"])
                request = videos.list_next(request, response)

            # normalizing data
            return [{"id": x["id"], "title":x["snippet"]["title"], "tags":x["snippet"].get("tags", [])} for y in videos_list for x in y]

            # response =  request.execute()
            # videos_list.append(response["items"])
            # # request = videos.list_next(request, response)
            # return [{"id": x["id"], "title":x["snippet"]["title"]} for y in videos_list for x in y]
        except RefreshError:
            print("Auth Error", platform_user_id)

    def transform(self, retrieved_data: dict):
        data = {
            "id": self._id_generator(retrieved_data),
            "name": self._name_generator(retrieved_data),
            "type": self._type_generator(),
            "source": self._source_generator(),
            "url": self._url_generator(retrieved_data),
            "tags": self._tags_generator(retrieved_data)
        }
        return data

    async def get_media(self, user_id, platform_token, refresh_token):
        dataRetrieved = self.retrieve(user_id, platform_token, refresh_token)
        if dataRetrieved is None:
            return
        dataTransformed = [self.transform(x) for x in dataRetrieved]
        # pprint.PrettyPrinter(indent=4).pprint(dataTransformed)
        # return (dataTransformed, user_id)
        self.callback((user_id, dataTransformed))


if __name__ == "__main__":
    youtubePlatform = youtubePlatform()
    data = youtubePlatform.get_media("", "", "", "")
    pprint.PrettyPrinter(indent=4).pprint(data)
    print(len(data[0]))
