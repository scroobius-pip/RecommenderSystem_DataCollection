import spotipy
import spotipy.util as util
from ..hash_gen import genHash
from ..types_constants import CONST_MUSIC

import pprint


class spotifyPlatform():

    def __init__(self, callback=lambda x:  pprint.PrettyPrinter(indent=4).pprint(x)):
        self.callback = callback

    def _id_generator(self, retrieved_data):

        return genHash(retrieved_data["id"])

    def _name_generator(self, retrieved_data):
        return retrieved_data["title"]

    def _type_generator(self):

        return CONST_MUSIC

    def _source_generator(self):
        return "spotify"

    def _tags_generator(self, retrieved_data):
        return [element.lower() for element in retrieved_data["tags"]]

    def _url_generator(self, retrieved_data):
        return "https://open.spotify.com/track/"+retrieved_data["id"]

    def retrieve(self, platform_user_id: str=None, platform_token: str=None, refresh_token: str=None):
        if(platform_user_id == None or platform_token == None):
            return None
        access_token = platform_token
        sp = spotipy.Spotify(auth=access_token)
        results = sp.current_user_saved_tracks()
        song_list = []
        while results:
            for item in results["items"]:
                track = item['track']
                song_list.append(track)
            if results["next"]:
                results = sp.next(results)
            else:
                break
        # pprint.PrettyPrinter(indent=4).pprint(song_list)
        return [{"id": song["id"], "title":song["name"], "tags":[artist["name"] for artist in song["artists"]]} for song in song_list]

    def transform(self, retrieved_data: dict):
        data = {
            "id": self._id_generator(retrieved_data),
            "name": self._name_generator(retrieved_data),
            "type": self._type_generator(),
            "source": self._source_generator(),
            "url": self._url_generator(retrieved_data),
            "tags": self._tags_generator(retrieved_data)
        }
        return data

    async def get_media(self, user_id, platform_token, refresh_token):
        dataRetrieved = self.retrieve(user_id, platform_token, refresh_token)
        if dataRetrieved is None:
            return
        dataTransformed = [self.transform(x) for x in dataRetrieved]

        self.callback((user_id, dataTransformed))


if __name__ == "__main__":
    spotifyPlatform = spotifyPlatform()
    data = spotifyPlatform.get_media(
        "simdi", "BQCXLCHgLewn8D6rMnepeBNmTTLyYltLN0mMhoiwkYH_qcMLdPSae8MScNJGthq8f22gGkX-xQ52Xwdmywx31CIfBPF9f3LY0bQPGuOp90XitW6PLeF7d4rqEU9R6DjkJHWwAfWv7Sekm7EPWOM", "")
    pprint.PrettyPrinter(indent=4).pprint(data)
    print(len(data[0]))
