from interface import Interface
from typing import List,Dict


class Input(Interface):
    """ Should return an array of User.

    User:
    {
        user_id:string,
        platforms:[Platform]
    }
    
    Platform:
    {
        platform_name:string,
        platform_user_id:string,
        platform_token:string
    }

    """
    def getUsers(self) -> List:
        pass

