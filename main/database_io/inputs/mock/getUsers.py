from interface import implements
from ..input import Input


class mockInput(implements(Input)):  # type: ignore
    def getUsers(self):
        return [
            {
                "user_id": "chisimdi",
                "platforms": [
                    {
                        "platform_name": "youtube",
                        "refresh_token": "1/VrYT1wBLmym5wmInKYnhWX7fo-9ofbBRW58OjlpDdC4",
                        "platform_token": "ya29.Glt5BRnFp4SLGSFeaV_6u-Kb6vGwYI_rJjLNy3D1UhnKDnqgHCA74kRbKpnhI0WPvBztSeEhKrYft2vtFWKnQf3M_ey7Qz7goAtpy8HIKs75weRyjTCLur1tbeI7"
                    },
                ]
            },
            {
                "user_id": "simdi",
                "platforms": [
                    {
                        "platform_name": "youtube",
                        "refresh_token": "1/-eBbrPZBea_Mkgj16Hn6oH6pQzxUddIT6z6MymhEMlE",
                        "platform_token": "ya29.GluBBUNydy_A9wNWzuEzo1BRkXD-ZaQk_xl_dpZPyDy-jr0UVpYrlRM-8JwfuRtT5sqhCtA6wWIxrxhv8iBz4GC7gm8sFNibjRRQSfe_S1ObiQ0eEqgH6HerdEHv"
                    }
                ]
            },
            {
                "user_id": "amomi",
                "platforms": [
                    {
                        "platform_name": "youtube",
                        "refresh_token": "1/2aO5Pdzn-CHB0G-iLW5XU00ksUCe2YhY2AM-vO7JivU",
                        "platform_token": "ya29.Glt5Bcq0XBo7pyy1DLkhR9P57RoZ_F044L8NXNmf-gvrgLMQAXJ3WcZbz3KGYRvik4adCvBCSrHzCO3MeN4qw5YAbS8gNT2mAbGZEdlOVKtbQAa3jEBWN-tAp3aF"
                    }
                ]
            },
            {
                "user_id": "obinna",
                "platforms": [
                    {
                        "platform_name": "youtube",
                        "refresh_token": "",
                        "platform_token": "ya29.Glt5BVO4dmkNk6SZ2X-sNSOphLWJmfJ7LsxZfSVyEALXmueKRGtfiHr72xEU8yfAYRRoiavW3jPZeaETiUgRc-Brs51ztsXUW6ijHCa3KV3Vdpi8vI70X9rAKzq2"
                    }
                ]
            },
            {
                "user_id": "stephan",
                "platforms": [
                    {
                        "platform_name": "youtube",
                        "refresh_token": "1/Hq2yZ0-UOFX87FQVpMuSuZsFWB7HILhfzC_ZNCukdHs",
                        "platform_token": "ya29.Glt5BZhfjyJeAdDO6XOGGHyB3-8oHv9skx7XNDgg1xP9HsL99dztrKjvJs3PfijXyDMsbQhz5q_Ctl6P4L4cVGEzE6nlj8wFlSH9rJWskG6LAndODp3-5WtYMWjA"
                    }
                ]
            },
            {
                "user_id": "connor",
                "platforms": [
                    {
                        "platform_name": "youtube",
                        "refresh_token": "1/EDf4m9nkm8WRYcnYoFcIspyHfR_fpi7_AeY0FFK4f_HmOIH-8NfpSDcyz3v1OlWO",
                        "platform_token": "ya29.Glt7BbzWxWGuHGEigBLkZGVjpjiq4eviFn9hR5bBPu8YJ75RV9bdkexmXyRzE5QXMT3_gSOGylYFVJXlYijNosP1S8e058kJZx1-rrXA-pDL_sfhCMm1fChp9YJW"
                    }
                ]
            },

        ]
