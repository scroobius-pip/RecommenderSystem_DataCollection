from py2neo import Node, Relationship, Graph
from ..config import neo4j_config


class neo4jOutput():
    def __init__(self):
        graph = Graph(host=neo4j_config["host"], bolt_port=neo4j_config["port"],
                      password=neo4j_config["password"], user=neo4j_config["username"])
        # graph = Graph(password="miriamisnice")
        self.instance = graph.begin()

    def export(self, data):
        print("exporting to neo4j")
        if(len(data) != 2):
            print("data parameter not of length 2")

            return
        user, many_media = data

        try:
            u = Node("User", id=str(user))
            for media in many_media:
                m = Node("Media")
                m["id"] = media["id"]
                m["name"] = media["name"]
                m["type"] = media["type"]
                m["source"] = media["source"]
                m["url"] = media["url"]
                for tag in media.get("tags", []):
                    t = Node("Tag", tag)
                    t["name"] = tag
                    has_tag_relationship = Relationship(m, "HASTAG", t)
                    self.instance.merge(has_tag_relationship)
                likes_relationship = Relationship(u, "LIKES", m)
                self.instance.merge(likes_relationship)
            self.instance.commit()
            print("exported to neo4j")
        except TypeError:
            print("Error exporting to database")


if __name__ == "__main__":
    media = ("god",
             [{'id': 'CxzDlURMZb',
               'name': 'A 12-year-old app developer | Thomas Suarez',
               'source': 'youtube',
               'type': 'video',
               'url': 'www.youtube.com/watch?v=Fkd9TWUtFm0'},
              {'id': 'QQdQf50MzA',
                 'name': 'What Solitary Confinement Does To The Brain',
                 'source': 'youtube',
                 'type': 'video',
                 'url': 'www.youtube.com/watch?v=KE71OCsjaW0'}])
    neo4jOutput().export(media)
