# from flask import Flask,request,jsonify
from quart import Quart, request, jsonify
from flask_cors import CORS, cross_origin
from .add_user_data import add_user_data
import asyncio
import os

app = Quart(__name__)
CORS(app)


@app.route("/add_user_data", methods=["POST"])
# @cross_origin()
async def get_user_data():
    try:
        request_user = await request.json
        user_access_tokens = request_user["users"]
        print(user_access_tokens)
        assert isinstance(user_access_tokens, list)
        await add_user_data(user_access_tokens)
        return "thanks"
    except TypeError:
        pass


if __name__ == '__main__':
    app.run(debug=False, port=os.environ.get("PORT", 5000), host='0.0.0.0')
